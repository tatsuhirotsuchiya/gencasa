package genCASA;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;

public class Outputer {

	BufferedWriter writer;

	Outputer(String filename) {
		this.writer = openFile(filename);
	}

	private BufferedWriter openFile(String filename) {
		BufferedWriter writer = null;
		if (filename == null) {
			// default: standard output
			return new BufferedWriter(new OutputStreamWriter(System.out));
		}

		try {
			writer = new BufferedWriter(new FileWriter(filename));
		} catch (IOException e) {
			// System.err.print(filename + " cannot be created.");
			// エラーを書き込めないので直接標準エラーへ
			System.err.print(Main.language == Main.Language.JP ? "出力ファイル"
					+ filename + "が作成できません．" : "Cannot create output file "
					+ filename);
			System.exit(1);
		}
		return writer;
	}

	void outputResult() {
		try {
			String firstline = "foo" + "\n";
			this.writer.write(firstline);
			this.writer.close();
		} catch (IOException e) {
			System.err.print("Cannot write the file");
		}
	}

}
