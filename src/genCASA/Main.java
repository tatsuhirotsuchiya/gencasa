package genCASA;


public class Main {
	static int randomSeed = -1;
	static String modelFile;
	static int numOfIterations = 1;
	static String seedFile;
	static String outputFile;
	static int strength = 2; // default strength

	static final int MAX_LEVEL = 63;

	static final int MAX_STRENGTH = 5;	

	// static final int Max_RandomSeed = 10;

	static boolean debugMode = false;

	enum Language {
		JP, EN
	};

	static Language language = Language.JP;

	// Start the whole process
	public static void main(String[] args) {

		try {
			// 
			// コマ引数処理
			String errorMessage = processCommandArgument(args);

			// エラー出力先設定
			Error.setOutputFile(outputFile);

			// コマンド引数でのエラー出力
			if (errorMessage != null)
				Error.printError(errorMessage);

			// モデル読み込み
			// System.err.println("starting reading model");
			InputFileData inputfiledata = Inputer.readModel(modelFile);
			
			// 
			System.out.println("# parameters\n" + inputfiledata.parameterList.size());
			System.out.println("# values");
			for (Parameter p: inputfiledata.parameterList) {
				System.out.print(p.value_name.size()+" ");
			}
			System.out.println();
			
			// 制約の解釈，表示 ←　これをつくる
			new ConstraintHandler(inputfiledata.parameterList, inputfiledata.constraintList);

		} catch (OutOfMemoryError e) {
			Error.printError(Main.language == Main.Language.JP ? "メモリ不足です．"
					: "Out of memory");
		} catch (Exception e) {
			Error.printError(Main.language == Main.Language.JP ? "プログラムが異常終了しました．"
					: "Abnormal termination");
		}

		//		long end = System.currentTimeMillis();
		//		System.err.println("time: " + (end - start) + "ms");
	}

	// コマンド引数処理
	private static String processCommandArgument(String[] args) {
		if (args.length == 0) {
			Error.printError("usage: java -jar Program.jar [-i input] [-o output] [-policy] ...");
		}

		// policyの表示
		if (args.length == 1 && args[0].equals("-policy")) {
			System.out
			.println("This software is distributed under the zlib license.\n"
					+ "Copyright (c) 2015, Tatsuhiro Tsuchiya\n"
					+ "This software is provided 'as-is', without any express or implied \n"
					+ "warranty. In no event will the authors be held liable for any damages \n"
					+ "arising from the use of this software. \n"
					+ "\n"
					+ "Permission is granted to anyone to use this software for any purpose, \n"
					+ "including commercial applications, and to alter it and redistribute it \n"
					+ "freely, subject to the following restrictions: \n"
					+ " \n"
					+ "   1. The origin of this software must not be misrepresented; you must not \n"
					+ "   claim that you wrote the original software. If you use this software \n"
					+ "   in a product, an acknowledgment in the product documentation would be \n"
					+ "   appreciated but is not required. \n"
					+ "   \n"
					+ "   2. Altered source versions must be plainly marked as such, and must not be \n"
					+ "   misrepresented as being the original software. \n"
					+ "   \n"
					+ "   3. This notice may not be removed or altered from any source \n"
					+ "   distribution. \n");
			System.exit(0);
		}

		// エラー表示を出力ファイルが指定されるまで遅らせる
		String errorMessage = null;

		for (int i = 0; i + 1 < args.length; i += 2) {
			String option = args[i];
			String str = args[i + 1];
			if (option.equals("-i")) {
				modelFile = str;
			} else if (option.equals("-o")) {
				outputFile = str;
			} else if (option.equals("-c")) {
				if (str.equals("all")) {
					// 全網羅
					strength = -1;
				} else {
					try {
						strength = Integer.parseInt(str);
					} catch (NumberFormatException e) {
						// Error.printError("invalid number");
						errorMessage = Main.language == Main.Language.JP ? "網羅度に無効な値が指定されています．"
								: "Invalid strength";
						continue;
					}
					if (strength < 2 || MAX_STRENGTH < strength) {
						// Error.printError("invalid strength");
						errorMessage = Main.language == Main.Language.JP ? "網羅度に無効な値が指定されています．"
								: "Invalid strength";
						continue;
					}
				}
			}
			// 繰り返し数
			else if (option.equals("-debug")) {
				debugMode = true;
				// 次の引数はダミー
			} else if (option.equals("-lang")) {
				if (str.matches("JP|jp")) {
					Main.language = Main.Language.JP;
				} else if (str.matches("EN|en")) {
					Main.language = Main.Language.EN;
				} else {
					errorMessage = "無効な言語が指定されています (Invalid Language)";
					continue;
				}
			} else {
				// Error.printError("Invalid option");
				errorMessage = Main.language == Main.Language.JP ? "無効なオプションが指定されています．"
						: "Invalid option";
				continue;
			}
		}

		return errorMessage;
	}
}

