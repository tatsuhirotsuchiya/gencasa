package genCASA;

import java.util.List;


public class ConstraintHandler {

	int numOfBDDvariables;
	
	// 制約の解釈，表示 
	public ConstraintHandler(PList parameterList, List<Node> constraintList) {

		// boolean 変数の総数を計算
		// numOfBooleanVariable = computeNumOfBooleanVariables
		
		// contrainListから、ノードを呼ぶ
		setBddConstraint(constraintList);
	}


	private void setBddConstraint(List<Node> constraintList) {

		// 制約式の論理積をとる
		for (Node n : constraintList) {		
			// int g = n.evaluate(bdd, parameters);
			String str = n.evaluate();
			// for debug
			System.out.println(str);
		}
		return;
	}
}
